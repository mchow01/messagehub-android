package org.owasp.boston.messenger;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import com.crashlytics.android.Crashlytics;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;

public class GetMessages extends AsyncTask<Void, Integer, JSONArray> {
    private Context copyOfContext;

    public GetMessages (Context context) {
        super();
        copyOfContext = context;
    }

    @Override
    protected JSONArray doInBackground(Void... params) {
        try {
            URL api = new URL(MainActivity.MESSAGES_API_URL);
            HttpURLConnection conn = (HttpURLConnection)api.openConnection();

            // See http://stackoverflow.com/questions/2492076/android-reading-from-an-input-stream-efficiently
            InputStream is = conn.getInputStream();
            BufferedReader r = new BufferedReader(new InputStreamReader(is));
            StringBuilder total = new StringBuilder();
            String line;
            while ((line = r.readLine()) != null) {
                total.append(line);
            }
            r.close();
            is.close();
            return new JSONArray(total.toString());
        }
        catch (MalformedURLException e) {
            Crashlytics.logException(e);
            Log.e("***** GetMessages doInBackground(): ", e.toString());
            return null;
        }
        catch (IOException e) {
            Crashlytics.logException(e);
            Log.e("***** GetMessages doInBackground(): ", e.toString());
            return null;
        }
        catch (JSONException e) {
            Crashlytics.logException(e);
            Log.e("***** GetMessages doInBackground(): ", e.toString());
            return null;
        }
    }

    @Override
    protected void onPostExecute(JSONArray messagesRaw) {
        JSONObject entry;
        int numMessages = 0, count = 0;
        ArrayList<Message> messages = new ArrayList<Message>();

        try {
            numMessages = messagesRaw.length();
            if (numMessages > 0) {
                while (count < numMessages) {
                    entry = messagesRaw.getJSONObject(count);
                    Message m = new Message();
                    m.setContent(entry.getString("content"));
                    m.setCreatedAt(entry.getString("created_at"));
                    m.setUpdatedAt(entry.getString("updated_at"));
                    m.setUsername(entry.getString("username"));
                    m.setId(entry.getInt("id"));
                    m.setActive(entry.getBoolean("active"));
                    m.setAppId(entry.getInt("app_id"));
                    messages.add(m);
                    count++;
                }
                Collections.reverse(messages);
                MainActivity.messagesView.setAdapter(new CustomListAdapter(copyOfContext, messages));
                MainActivity.progress.dismiss();
            }
        }
        catch (JSONException e) {
            Crashlytics.logException(e);
            Log.e("***** GetMessages doInBackground(): ", e.toString());
        }
    }
}
