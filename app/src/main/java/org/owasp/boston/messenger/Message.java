package org.owasp.boston.messenger;

import java.text.SimpleDateFormat;
import java.util.Locale;

import android.util.Log;
import com.crashlytics.android.Crashlytics;

public class Message {
	protected int id;
	protected String content;
	protected String username;
	protected String createdAt;
	protected String updatedAt;
	protected int appId;
	protected boolean active;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getCreatedAt() {
		// See http://stackoverflow.com/questions/4216745/java-string-to-date-conversion
		try {
			return(new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss z", Locale.ENGLISH).parse(createdAt).toString());
		}
		catch (Exception e) {
            Crashlytics.logException(e);
			Log.e("**** Message getCreatedAt(): ", e.toString());
			return createdAt;
		}
	}
	
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	
	public String getUpdatedAt() {
		try {
			return(new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss z", Locale.ENGLISH).parse(updatedAt).toString());
		}
		catch (Exception e) {
            Crashlytics.logException(e);
			Log.e("**** Message getUpdatedAt(): ", e.toString());
			return updatedAt;
		}
	}
	
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	public int getAppId() {
		return appId;
	}
	
	public void setAppId(int appId) {
		this.appId = appId;
	}
	
	public boolean isActive() {
		return active;
	}
	
	public void setActive(boolean active) {
		this.active = active;
	}
}
