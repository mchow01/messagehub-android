package org.owasp.boston.messenger;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.app.AlertDialog;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Button;
import android.widget.Toast;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import com.crashlytics.android.Crashlytics;

public class MainActivity extends Activity {
	static protected ListView messagesView;
	protected Button composeButton;
	protected Dialog popup;
	public static final String MESSAGES_API_URL = "https://messagehub.herokuapp.com/messages.json";
	static protected Toast whoops;
    protected static ProgressDialog progress;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Crashlytics.start(this);
        progress = new ProgressDialog(this);
        progress.setMessage(getString(R.string.progress_text));
        progress.show();
        setContentView(R.layout.activity_main);
		whoops = Toast.makeText(this, getString(R.string.whoops), Toast.LENGTH_SHORT);
		popup = createSendDialog();
		messagesView = (ListView)findViewById(R.id.messagesView);
		parseMessages();
		composeButton = (Button)findViewById(R.id.composeButton);
		composeButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				popup.show();
            }
		});
	}

	/* References:
	 * http://stackoverflow.com/questions/5235237/dialog-box-or-form-where-user-can-choose-to-enter-details-or-cancel
	 * http://developer.android.com/guide/topics/ui/dialogs.html
	 */
	private Dialog createSendDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		LayoutInflater inflater = this.getLayoutInflater();
		final View layout = inflater.inflate(R.layout.send_alertdialog, null);
		builder.setView(layout)
			.setTitle(R.string.dialog_title)
			.setPositiveButton(R.string.send, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					final EditText usernameBox = (EditText)layout.findViewById(R.id.username);
				    final EditText contentBox = (EditText)layout.findViewById(R.id.content);
                    new PostMessage().execute(usernameBox.getText().toString(),
                            contentBox.getText().toString());
                    parseMessages();
				}
			})
			.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.dismiss();
				}
			});
		return builder.create();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_refresh:
				parseMessages();
				return true;
			case R.id.action_about:
				Toast.makeText(this, R.string.about_text, Toast.LENGTH_LONG).show();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

    protected void parseMessages() {
        new GetMessages(getApplicationContext()).execute();
    }
}
