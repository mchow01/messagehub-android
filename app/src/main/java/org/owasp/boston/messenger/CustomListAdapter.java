package org.owasp.boston.messenger;

import java.util.ArrayList;

import android.*;
import android.R;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CustomListAdapter extends BaseAdapter {
	private Context context;
	private ArrayList<Message> messages;
	
	public CustomListAdapter(Context context, ArrayList<Message> messages) {
        this.context = context;
        this.messages = messages;
	}

	@Override
	public int getCount() {
		return messages.size();
	}

	@Override
	public Object getItem(int position) {
		return messages.get(position);
	}

	@Override
	public long getItemId(int position) {
		return messages.get(position).id;
	}

	@Override
	public boolean areAllItemsEnabled() {
		return false;
	}
	
	@Override
	public boolean isEnabled (int position) {
		return false;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = (View)inflater.inflate(R.layout.simple_list_item_2, null);
		}
		else {
			row = (View)convertView;
		}
		TextView v = (TextView)row.findViewById(android.R.id.text1);
        v.setTextColor(Color.BLACK);
		v.setText(messages.get(position).content);
		v = (TextView) row.findViewById(android.R.id.text2);
        v.setTextColor(Color.BLACK);
        v.setText(messages.get(position).getUsername() + " (" + messages.get(position).getCreatedAt() + ")");
		return row;
    }
}
